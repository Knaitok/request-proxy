<?php
// загрузка автозагрузчика
require_once 'lib/Twig/Autoloader.php';
Twig_Autoloader::register();

// место где будут хранятся шаблоны Twig
$loader = new Twig\Loader\FilesystemLoader('/views');

// инициализация самого движка
$twig = new Twig_Environment($loader, array(
    'cache'       => 'compilation_cache',
    'auto_reload' => true
));

