var formData = new FormData();
var servText = document.querySelector('#result');
var encrypt = document.querySelector('#encrypt');
var ws = document.querySelector('#ws');

document.querySelector('#save').addEventListener('click', function(e){
    formData.append('host',document.forms.save_bd.host.value);
    formData.append('description',document.forms.save_bd.description.value);
    formData.append('http',document.forms.save_bd.http.value);
    formData.append('https',document.forms.save_bd.https.value);
    if(encrypt.checked){
        formData.append('encrypt','true');
    }else{
        formData.append('encrypt','false');
    }
    if(ws.checked){
        formData.append('ws','true');
    }else{
        formData.append('ws','false');
    }
    e.preventDefault();
    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'test.php');

    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4 && xhr.status === 200){
            servText.innerHTML = xhr.responseText;
        }
    }
    xhr.send(formData);
});
