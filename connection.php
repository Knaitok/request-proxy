<?php

class Connection
{
    private $host;
    private $database;
    private $user;
    private $password;
    private $table;

    public function __construct()
    {
        $this->host = 'localhost';
        $this->user = 'userproxy';
        $this->database = 'dbproxy';
        $this->password = '7JJ4BKje6KL}Fc8';
        $this->table = 'main';
    }

    public function createConnectionToDB()
    {
        $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
        if ($conn->connect_errno) {
            echo 'connect error: '.$conn->connect_error;
        } else {
        return $conn;
        }
    }

    public function applyQuery($host, $description, $http, $https, $redirect, $encrypt, $ws)
    {

        $connDB = $this->createConnectionToDB();
        $query = 'INSERT INTO '.$this->table.' (host,description,http,https,redirect,letsEncrypt,websocket) values ("'.$host.'","'.$description.'","'.$http.'","'.$https.'","'.$redirect.'","'.$encrypt.'","'.$ws.'");';

        $result = $connDB->query($query);

        if ($connDB->connect_errno) {
        echo 'connect error: '.$connDB->connect_error;
        }
        $this->select_db($connDB);
        mysqli_close($connDB);
    }

    public function select_db($conn){

        $query = 'SELECT * FROM '.$this->table;
        $result = $conn->query($query);
        if ($conn->connect_errno) {
            echo 'connect error: '.$conn->connect_error;
        }else{
            echo "<div id = 'result'>";
            echo "<table class = 'table table-hover'>";
            while($row = mysqli_fetch_array($result)){
                echo '<tr>';
                echo '<td>'.$row['host']. "</td>";
                echo '</tr>';
            }
            echo "</table>";
            echo "</div>";
        }
    }
}
